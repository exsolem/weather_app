import React from 'react';
import logo from './logo.svg';
import './App.css';
import Weather from './Weather/Weather';
import { CSSTransition } from 'react-transition-group';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayWeather: false
    }
    this.hide = this.hide.bind(this);
  }
  hide(){
    this.setState({
      displayWeather:!this.state.displayWeather
    })
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <CSSTransition
            in={this.state.displayWeather}
            timeout={200}
            classNames='Weather-node'
            unmountOnExit
          >
            <Weather hide = {this.hide}/>
          </CSSTransition>
          <button className='btn btn-outline-secondary'
            onClick={() => this.setState({ displayWeather: !this.state.displayWeather })}>
            {this.state.displayWeather ? 'Hide Weather' : 'Show weather'}
          </button>
        </header>
      </div>
    );
  }
}

export default App;
