import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import './Weather.css'
class Weather extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            weather: null,
            timezone: null,
            visibility: null,
            humidity: null,
            pressure: null,
            temp: null,
            temp_max: null,
            temp_min: null,
            name: null,
            sunrise: null,
            sunset: null,
            country: null,
            sky: null,
            icon: null,
            main: null,
            wind_speed: null,
            wind_deg: null,
            currentTime: null
        }
        this.getWeather = this.getWeather.bind(this);
        this.refresh = this.refresh.bind(this);
    }
    getWeather() {
        const weatherRequest = new XMLHttpRequest();
        let weather, lat, lon;

        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log('i`m in');
                lat = Math.round(position.coords.latitude * 100) / 100;
                lon = Math.round(position.coords.longitude * 100) / 100;
                console.log(lat + '------' + lon);
                weatherRequest.open(
                    'GET',
                    `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=dd52b4fc8e3c179c1f657e95a34e5427`,
                    true)
                weatherRequest.send();
                weatherRequest.onreadystatechange = () => {
                    if (weatherRequest.readyState === 4 && weatherRequest.status === 200) {
                        weather = JSON.parse(weatherRequest.response);
                        console.log(weather);
                        this.setState({
                            weather: weather,
                            timezone: `+${new Date(weather.timezone).getHours()}`,
                            visibility: weather.visibility,
                            humidity: weather.main.humidity,
                            pressure: weather.main.pressure,
                            temp: Math.round(weather.main.temp - 273.15),
                            temp_max: Math.round(weather.main.temp_max - 273.15),
                            temp_min: Math.round(weather.main.temp_min - 273.15),
                            icon: weather.weather[0].icon,
                            sky: weather.weather[0].description,
                            wind_deg: weather.wind.deg
                        })
                    }
                    else {
                        console.log(`${weatherRequest.status}:${weatherRequest.statusText}`);
                    }
                }
            },
            (err) => {
                console.log("Error Detected")
                weatherRequest.open('GET', 'http://api.openweathermap.org/data/2.5/weather?q=Kiev&APPID=dd52b4fc8e3c179c1f657e95a34e5427', true);
                weatherRequest.send();
                weatherRequest.onreadystatechange = () => {
                    if (weatherRequest.readyState === 4 && weatherRequest.status === 200) {
                        weather = JSON.parse(weatherRequest.response);
                        console.log(weather);
                        this.setState({
                            weather: weather,
                            timezone: `+${new Date(weather.timezone).getHours()}`,
                            visibility: weather.visibility,
                            humidity: weather.main.humidity,
                            pressure: weather.main.pressure,
                            temp: Math.round(weather.main.temp - 273.15),
                            temp_max: Math.round(weather.main.temp_max - 273.15),
                            temp_min: Math.round(weather.main.temp_min - 273.15),
                            icon: weather.weather[0].icon,
                            sky: weather.weather[0].description,
                            wind_deg: weather.wind.deg
                        })
                    }
                    else {
                        console.log(`${weatherRequest.status}:${weatherRequest.statusText}`);
                    }
                };
            }, { maximumAge: 600000 });
        //weatherRequest.open('GET', 'http://api.openweathermap.org/data/2.5/weather?q=Kiev&APPID=dd52b4fc8e3c179c1f657e95a34e5427', true);


    }
    componentWillMount() {
        console.log(this.props);
        if (!this.state.weather) this.getWeather();
    }
    refresh(){
        this.props.hide();
        this.getWeather();
        setTimeout(() => this.props.hide(),200);
    }
    render() {
        return (
            <div className='Weather h5'>
                <img src={`http://openweathermap.org/img/wn/${this.state.icon}@2x.png`} className='weather__icon' alt="icon" />
                <p className='text-info Weather__p'>Sky: <span className='Weather__info'>{this.state.sky}</span></p>
                <p className='text-info Weather__p'>Current temp: <span className='Weather__info'>{this.state.temp}&#8451;</span></p>
                <p className='text-info Weather__p'>Day t <sup>max</sup> : <span className='Weather__info'>{this.state.temp_max}&#8451;</span></p>
                <p className='text-info Weather__p'>Day t <sup>min</sup> : <span className='Weather__info'>{this.state.temp_min}&#8451;</span></p>
                <p className='text-info Weather__p'>Pressure: <span className='Weather__info'>{this.state.pressure}mm</span></p>
                <p className='text-info Weather__p'>Humidity: <span  role='img' aria-label='water' className='Weather__info'>&#128167;{this.state.humidity}%</span></p>
                <p className='text-info Weather__p'>Wind:
                     <img
                        src="http://s1.iconbird.com/ico/2014/1/598/w128h1281390846431downcircular128.png"
                        alt="arrow"
                        className='Weather__wind-arrow'
                        style={{ transform: `rotate(${this.state.wind_deg}deg)` }} />
                </p>
                <button onClick={this.refresh} className='btn btn-outline-light'>Refresh</button>
            </div>);
    }
}
export default Weather;